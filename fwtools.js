(function (){

	var initHtml = document ;
	var isWFocus = true ;
	var isWBlur = false ;
	var tmove = false ;
	var pmove = false ;

	window.onfocus = function () {
		isWFocus = true ;
		isWBlur = false ;
	}

	window.onblur = function () {
		isWFocus = false ;
		isWBlur = true ;
	}

	window.onpointermove = function (){
		pmove = true ;
	};

	window.ontouchmove = function (){
		tmove = true ;
	};
	
	const url = window.location.href;
	const proto = window.location.protocol;

	window.setInterval(function () {
		console.log(
			' tmove : ', tmove,
			' pmove : ', pmove
			);
		
		if((pmove === false)){
			// console.log('refresh');
			window.location.assign(url);
		}

		setTimeout(function(){
			tmove = false ;
			pmove = false ;
		}, 4000);

	}, 1000);

})();